import unittest
import os
from unittest.mock import MagicMock
from lib.image_reader import get_image_names_in_path, read_images, 

mock_pic_path = r'C:\Users\Patrick\Dropbox\Programming\PyExifStats\unittest\mock_pics'

class TestFileLoading(unittest.TestCase):

    def test_load_some_files(self):
        pic_paths = __get_test_pic_paths()
        self.assertEqual(get_image_names_in_path(mock_pic_path), pic_paths)

    def test_load_from_invalid_path(self):
        path = r'C:\unexisting_path\unexisting_folder'
        self.assertEqual(get_image_names_in_path(path), [])

    def test_read_pics(self):
        pic_paths = __get_test_pic_paths()
        list_of_exif_blobs = read_images(pic_paths)
        self.assertIsInstance(list_of_exif_blobs, list)
        self.assertEqual(len(list_of_exif_blobs), 3)

    def test_save_exif_blobs(self):
        image_exifs = [{'len': 3, 'focus': 55}, ]
        exif_path = os.path.join(mock_pic_path, 'exif_folder')
        os.path.
        save_image_exifs(image_exifs)
        self.assert(os.path.exists())

def __get_test_pic_paths():

    pic_paths = [os.path.join(mock_pic_path, filename) for filename in ['2020-09-04-17h29m46.JPG', '2020-09-05-13h06m51.JPG', '2020-09-05-13h34m47.JPG']]
    return pic_paths





if __name__ == '__main__':
    unittest.main()