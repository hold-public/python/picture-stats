import exif
import random
import matplotlib.pyplot as plt

from lib.image_reader import get_image_names_in_path
from lib.neuronal_network import ObjectFinder
from lib.utils import print_green, print_yellow


def do_exif_stats():
    path = r'C:\Users\Patrick\Desktop\test_imgs_people' #'F:\\Pictures\\DSLM\\2020\\01-03'
    img_filepath_list_all = get_image_names_in_path(path)
    img_filepath_list = img_filepath_list_all #random.choices(img_filepath_list_all, k=5)

    my_object_finder = ObjectFinder()
    image_exifs = []
    for image_filepath in img_filepath_list:
        print(f'loading img {image_filepath}..')
        if my_object_finder.is_object_in_img(image_filepath, ['man', 'woman']):
            print_green(f'seem humane!')
            with open(image_filepath, 'rb') as image_file:
                image_exif = exif.Image(image_file)
                image_exifs.append(image_exif)
        else:
            print_yellow(f'seem object-like!')

    focal_lengths_per_lens_model = dict()
    for image_exif in image_exifs:
        lens_model = image_exif.lens_model
        focal_length = image_exif.focal_length
        if lens_model in focal_lengths_per_lens_model:
            focal_lengths_per_lens_model[lens_model].append(focal_length)
        else:
            focal_lengths_per_lens_model[lens_model] = [focal_length]

    for lens_model in focal_lengths_per_lens_model:
        if lens_model == 'E 18-135mm F3.5-5.6 OSS':
            fig = plt.figure()
            plt.hist(focal_lengths_per_lens_model[lens_model], bins=20, range=(18, 150))
            plt.xlabel('focal length / mm')
            plt.ylabel('occurrence')
            plt.title(f'lens: {lens_model}')
            plt.show()


do_exif_stats()