""" uses a neuronal network to find stuff """

# from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.applications.vgg16 import VGG16

from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np
import matplotlib.pyplot as plt



class ObjectFinder():
    def __init__(self, min_probability_pc, debugging):
        self.min_probability_pc = min_probability_pc
        self.debugging = debugging
        # self.model = ResNet50(weights='imagenet')
        self.model = VGG16(weights='imagenet')

    def is_object_in_img(self, img_path, object_names_target):
        img = image.load_img(img_path, target_size=(224, 224))
        
        img_array = image.img_to_array(img)
        img_array_expanded = np.expand_dims(img_array, axis=0)
        img_array_expanded_preprocessed = preprocess_input(img_array_expanded)

        preds = self.model.predict(img_array_expanded_preprocessed)
        # decode the results into a list of tuples (class, description, probability)
        # (one such list for each sample in the batch)
        decoded_predictions = decode_predictions(preds, top=30)[0]

        for decoded_prediction in decoded_predictions:
            object_name = decoded_prediction[1]
            probability_pc = decoded_prediction[2] * 100
            if self.debugging:
                print(f'There\'s a {probability_pc:4.1f} % probability it\'s a {object_name}')
            for object_name_target in object_names_target:
                if object_name_target in object_name:
                    if probability_pc > self.min_probability_pc: # percent
                        self.__debug(img_array, is_match=True)
                        return True

        self.__debug(img_array, is_match=False)
        return False

    def __debug(self, img_array, is_match):
        if self.debugging:
            if is_match:
                print('match!')
            else:
                print('no match')
            plt.imshow(img_array / 255.)
            plt.show()