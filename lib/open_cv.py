import cv2
from matplotlib import pyplot as plt
import numpy as np

    
def is_bottle_in_img(img_file_path, debugging):
    img_bgr_big_size = cv2.imread(img_file_path)
    img_bgr = cv2.resize(img_bgr_big_size, (0, 0), fx=0.2, fy=0.2)

    orange_mask = __get_orange_mask(img_bgr)

    if debugging:
        img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
        img_rgb_just_orange = cv2.bitwise_and(img_rgb, img_rgb, mask=orange_mask)
        img_hsv = __get_std_hsv(img_bgr)
        img_hue = img_hsv[:, :, 0]
        img_sat = img_hsv[:, :, 1]
        img_val = img_hsv[:, :, 2]


        subplot_index = 0
        plt.figure(figsize=(17.0, 10.0))

        subplot_index += 1
        plt.subplot(2, 3, subplot_index)
        plt.imshow(img_rgb)
        plt.title('img_rgb')

        subplot_index += 1
        plt.subplot(2, 3, subplot_index)
        plt.imshow(img_hue, cmap='gray')
        plt.title('hue')

        subplot_index += 1
        plt.subplot(2, 3, subplot_index)
        plt.imshow(img_sat, cmap='gray')
        plt.title('sat')

        subplot_index += 1
        plt.subplot(2, 3, subplot_index)
        plt.imshow(img_val, cmap='gray')
        plt.title('val')

        subplot_index += 1
        plt.subplot(2, 3, subplot_index)
        plt.imshow(orange_mask, cmap='gray')
        plt.title('orange_mask')

        subplot_index += 1
        plt.subplot(2, 3, subplot_index)
        plt.imshow(img_rgb_just_orange)
        plt.title('just orange parts')

        plt.show()

    has_right_amount_of_orange = __has_right_amount_of_orange(orange_mask)
    if not has_right_amount_of_orange:
        return False

    has_one_big_orange_blob = __has_one_big_orange_blob(orange_mask, debugging)
    if not has_one_big_orange_blob:
        return False

    return True


def __get_orange_mask(img_bgr):
    img_ocv_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)

    lower_orange = __hsv_to_ocv_hsv(np.array([8, 150, 30]))
    upper_orange = __hsv_to_ocv_hsv(np.array([32, 255, 255]))

    orange_mask = cv2.inRange(img_ocv_hsv, lower_orange, upper_orange)

    kernel = np.ones((5, 5), np.uint8)
    N_CYCLES = 2
    orange_mask = cv2.erode(orange_mask, kernel, iterations=N_CYCLES)
    orange_mask = cv2.dilate(orange_mask, kernel, iterations=N_CYCLES)

    return orange_mask


def __has_one_big_orange_blob(is_orange, debugging):
    small_blobs = __get_blobs(is_orange, rel_thres_min=0.0005, rel_thres_max = 0.01)
    big_blobs = __get_blobs(is_orange, rel_thres_min=0.01, rel_thres_max = 10000.0)

    if debugging:
        if small_blobs:
            # Draw detected blobs as red circles.
            # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
            img_with_keypoints = cv2.drawKeypoints(is_orange, small_blobs, np.array([]), (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            # Show keypoints
            cv2.namedWindow("small_blobs", cv2.WINDOW_GUI_NORMAL)
            cv2.imshow("small_blobs", img_with_keypoints)

        if big_blobs:
            # Draw detected blobs as red circles.
            # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
            img_with_keypoints = cv2.drawKeypoints(is_orange, big_blobs, np.array([]), (0, 0, 255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

            # Show keypoints
            cv2.namedWindow("big_blobs", cv2.WINDOW_GUI_NORMAL)
            cv2.imshow("big_blobs", img_with_keypoints)

    has_one_big_orange_blob = len(small_blobs) <= 5 and 1 <= len(big_blobs) <= 3
    return has_one_big_orange_blob

def __has_right_amount_of_orange(orange_mask):
    nr_of_orange_pixels = cv2.countNonZero(orange_mask)
    total_pixels = __get_total_pixels(orange_mask)

    ratio = nr_of_orange_pixels / total_pixels
    return bool(ratio > 0.01 and ratio < 0.05)




def __hsv_to_ocv_hsv(hsv):
    cv2_hsv = hsv
    cv2_hsv[0] *= (179 / 360)
    return cv2_hsv

def __get_total_pixels(img):
    img_shape = img.shape
    total_pixels = img_shape[0] * img_shape[1]
    return total_pixels

def __get_std_hsv(img_bgr):
    img_hsv_180 = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)
    img_hsv = img_hsv_180.astype('uint32')

    img_hsv[:, :, 0] *= 2
    return img_hsv


def __get_blobs(img, rel_thres_min, rel_thres_max):
    params = cv2.SimpleBlobDetector_Params()

    params.minThreshold = 127
    params.maxThreshold = 255

    total_pixels = __get_total_pixels(img)

    min_area = total_pixels * rel_thres_min
    max_area = total_pixels * rel_thres_max
    params.filterByArea = True
    params.minArea = min_area
    params.maxArea = max_area

    params.filterByCircularity = True
    params.minCircularity = 0.1
    params.maxCircularity = 1.0

    params.filterByConvexity = False
    # params.minConvexity = 0.87

    params.filterByInertia = False
    # params.minInertiaRatio = 0.01

    params.filterByColor = False

    detector = cv2.SimpleBlobDetector_create(params)

    keypoints = detector.detect(img)

    return keypoints