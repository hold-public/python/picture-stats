class bcolors:
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'

def print_green(text):
    print(f"{bcolors.GREEN}{text}{bcolors.ENDC}")
    
def print_yellow(text):
    print(f"{bcolors.YELLOW}{text}{bcolors.ENDC}")

def print_red(text):
    print(f"{bcolors.RED}{text}{bcolors.ENDC}")
