""" finds biotta bottles """

import shutil

from lib.image_reader import get_image_names_in_path, make_empty_folder
from lib.neuronal_network import ObjectFinder
from lib.open_cv import is_bottle_in_img
from lib.utils import print_green, print_yellow

METHOD = 'nn'

class BottleFinder():
    """ interface class """
    def __init__(self, method):
        """ init """
        self.method = method
        if self.method == 'ocv':
            pass
        elif self.method == 'nn':
            self.bottle_finder = ObjectFinder(min_probability_pc=2, debugging=DEBUGGING)
        else:
            raise Exception
    def is_bottle_in_img(self, img_file_path):
        """ returns true if image in path is a bottle """
        if self.method == 'ocv':
            return is_bottle_in_img(img_file_path=img_file_path, debugging=DEBUGGING)
        elif self.method == 'nn':
            return self.bottle_finder.is_object_in_img(img_file_path, 
            [
                'beer_bottle',
                'missile',
                'pop_bottle',
                'sunscreen',
                'water_bottle',
                'wine_bottle',
            ])
        else:
            raise Exception

def find_bottles(debugging, method, input_folder_path, output_folder_path_match, output_folder_path_non_match):
    """ looks for orange biotta bottles """
    print('deleting old output..')
    make_empty_folder(output_folder_path_match)
    make_empty_folder(output_folder_path_non_match)
    print('searching for images..')
    img_file_paths_all = get_image_names_in_path(input_folder_path)
    img_file_paths = img_file_paths_all
    nr_of_imgs = len(img_file_paths)
    print(f'{nr_of_imgs} image(s) found!')
    nr_of_orange_imgs = 0
    print('analyzing images..')
    my_bottle_finder = BottleFinder(method=method)
    for img_file_path in img_file_paths:
        if my_bottle_finder.is_bottle_in_img(img_file_path):
            shutil.copy2(img_file_path, output_folder_path_match)
            nr_of_orange_imgs += 1
            if debugging:
                print_green(f'image "{img_file_path}" looks biotta-like!')
        else:
            if debugging:
                print_yellow(f'img "{img_file_path}" ain\'t no bottle!')
                shutil.copy2(img_file_path, output_folder_path_non_match)
    print(f'finished! {nr_of_orange_imgs} orange imgages found!')

    match_ratio = nr_of_orange_imgs / nr_of_imgs
    return match_ratio


if __name__ == '__main__':
    UNKNOWN_FOLDER_PATH =  r'F:\Pictures\DSLM' 
    REAL_BOTTLE_FOLDER_PATH = r'C:\Users\Patrick\Desktop\test_imgs\bottle_pics'
    REAL_NON_BOTTLE_FOLDER_PATH = r'C:\Users\Patrick\Desktop\test_imgs\non_bottle_pics'

    CLASSIFIED_AS_BOTTLE_PATH = r'C:\Users\Patrick\Desktop\classified_as_bottle_imgs'
    CLASSIFIED_AS_NON_BOTTLE_PATH = r'C:\Users\Patrick\Desktop\classified_as_non_bottle_imgs'
    DEBUGGING = False

    sensitivity_ocv = find_bottles(debugging=DEBUGGING, method='ocv', input_folder_path=REAL_BOTTLE_FOLDER_PATH, output_folder_path_match=CLASSIFIED_AS_BOTTLE_PATH, output_folder_path_non_match=CLASSIFIED_AS_NON_BOTTLE_PATH)
    specificity_ocv = 1. - find_bottles(debugging=DEBUGGING, method='ocv', input_folder_path=REAL_NON_BOTTLE_FOLDER_PATH, output_folder_path_match=CLASSIFIED_AS_BOTTLE_PATH, output_folder_path_non_match=CLASSIFIED_AS_NON_BOTTLE_PATH)
    sensitivity_nn = find_bottles(debugging=DEBUGGING, method='nn', input_folder_path=REAL_BOTTLE_FOLDER_PATH, output_folder_path_match=CLASSIFIED_AS_BOTTLE_PATH, output_folder_path_non_match=CLASSIFIED_AS_NON_BOTTLE_PATH)
    specificity_nn = 1. - find_bottles(debugging=DEBUGGING, method='nn', input_folder_path=REAL_NON_BOTTLE_FOLDER_PATH, output_folder_path_match=CLASSIFIED_AS_BOTTLE_PATH, output_folder_path_non_match=CLASSIFIED_AS_NON_BOTTLE_PATH)

    print('ocv:')
    print_green(f'{sensitivity_ocv:.1%} sensitivity')
    print_yellow(f'{specificity_ocv:.1%} specificity')

    print('nn:')
    print_green(f'{sensitivity_nn:.1%} sensitivity')
    print_yellow(f'{specificity_nn:.1%} specificity')